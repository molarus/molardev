<<<<<<< HEAD
# Deploy to AWS CodeDeploy
An example script and configuration for updating an existing [AWS CodeDeploy](https://aws.amazon.com/codedeploy/) application with BitBucket Pipelines. This example deploys a Hello World application to an existing AWS CodeDeploy Application Deployment Group.

## How To Use It
* Optional:  [Create an S3 bucket](http://docs.aws.amazon.com/AmazonS3/latest/gsg/CreatingABucket.html) to hold application revisions if you do not currently have one.
* Optional:  [Setup a demo application](http://docs.aws.amazon.com/codedeploy/latest/userguide/getting-started-walkthrough.html) with AWS CodeDeploy if you do not already have one.
* Add the required Environment Variables below in Build settings of your Bitbucket repository.
* Copy `codedeploy_deploy.py` to your project.
* Copy `bitbucket-pipelines.yml` to your project.
    * Or use your own, just be sure to include all steps in the sample yml file.
* If you followed the Getting Started guide above and are using the sample application included in this repository, copy the `scripts` directory, `appspec.yml` file, and `index.html` file to your project.

## Required Permissions in AWS
It is recommended you [create](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) a separate user account used for this deploy process.  This user should be associated with a group that has the `AWSCodeDeployFullAccess` and `AmazonS3FullAccess` [AWS managed policy](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html) attached for the required permissions to upload a new application revision and execute a new deployment using AWS CodeDeploy.

Note that the above permissions are more than what is required in a real scenario. For any real use, you should limit the access to just the AWS resources in your context.

## Required Environment Variables
* `AWS_SECRET_ACCESS_KEY`:  Secret key for a user with the required permissions.
* `AWS_ACCESS_KEY_ID`:  Access key for a user with the required permissions.
* `AWS_DEFAULT_REGION`:  Region where the target AWS CodeDeploy application is.
* `APPLICATION_NAME`: Name of AWS CodeDeploy application.
* `DEPLOYMENT_CONFIG`: AWS CodeDeploy Deployment Configuration (CodeDeployDefault.OneAtATime|CodeDeployDefault.AllAtOnce|CodeDeployDefault.HalfAtATime|Custom).
* `DEPLOYMENT_GROUP_NAME`: Name of the Deployment group in the application.
* `S3_BUCKET`:  Name of the S3 Bucket where source code to be deployed is stored.

# License
Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Note: Other license terms may apply to certain, identified software files contained within or distributed with the accompanying software if such terms are included in the directory containing the accompanying software. Such other license terms will then apply in lieu of the terms of the software license above.
=======
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
>>>>>>> c768cb796f13ba1713b76dc2e24d5e551b56c5b4
